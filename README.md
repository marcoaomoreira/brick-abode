# Brick-Abode

Full Project Scope: https://docs.google.com/document/d/1wOnvl6jhHtrYhqRx-9XgFLLfibTg-6Fc86Qj-bOz49Q/edit#
Mock kick-off presentation: https://docs.google.com/presentation/d/1S4Ghr9sEjjnfLFMzSLujJpzP3wNIkDfCBvwRfFgzz9s/edit#slide=id.p

This repository is for team project to automate credentials switching


## Introduction

Our client would like to create Ansible playbooks to automate credentials switching on a set of network devices located at the EMEA region.
The client is located at the US, and although the stakeholders are skilled network engineers, they don’t have any knowledge on software development. They will be the main consumer of the tools.
The development team is composed of 2 developers (one senior and one junior) and a project manager.

 
## Requirements

Create a set of Ansible playbooks to automate credential switching on a set of network devices. The credentials and devices are listed below:

Network devices:
- Opengear devices
- Juniper devices
- Cisco devices
Credentials
- Root/admin password
- BGP Authentication-key
- RADIUS key
- SMTP community string




## Milestonesf
Milestone 1: Research and server setup
Deliverable 1:Document with the specifications, installation process and dependencies to execute the Ansible playbooks

Milestone 2: Implementation of missing Ansible modules
Deliverable 2: Ansible module for Opengear devices


Milestone 3: Implementation of the Ansible playbooks
Deliverable 3: Ansible playbooks for:
- Root/admin password (Opengear, Cisco and Juniper devices)
- BGP Authentication-key (Opengear, Cisco and Juniper devices)
- RADIUS key (Opengear, Cisco and Juniper devices)
- SMTP community string (Opengear, Cisco and Juniper devices)

Milestone 4: Testing and validation
Deliverable 4: Modules and playbooks tested and validated

Milestone 5: Documentation and delivery
Deliverable 5: A detailed written report

